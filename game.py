import pygame, gui

NUMKEYS = 300
NUMBUTTONS = 6

state = None
running = True
key_down, key_pressed, key_released, mouse_down, mouse_pressed, mouse_released = [], [], [], [], [], []

def get_key_down(key):
	global key_down
	return key_down[key]

def get_key_pressed(key):
	global key_pressed
	return key_pressed[key]

def get_key_released(key):
	global key_released
	return key_released[key]

def get_mouse_down(button):
	global mouse_down
	return mouse_down[button]

def get_mouse_pressed(button):
	global mouse_pressed
	return mouse_pressed[button]

def get_mouse_released(button):
	global mouse_released
	return mouse_released[button]

def get_mouse_pos():
	return pygame.mouse.get_pos()

def set_mouse_pos(pos):
	pygame.mouse.set_pos(pos)

def get_state():
	global state
	return state

def set_state(new_state):
	global state
	state = new_state

def quit():
	global running
	running = False

def run(new_state):
	global running, state
	global key_down, key_pressed, key_released, mouse_down, mouse_pressed, mouse_released
	key_down = [False]*NUMKEYS
	key_pressed = [False]*NUMKEYS
	key_released = [False]*NUMKEYS
	mouse_down = [False]*NUMBUTTONS
	mouse_pressed = [False]*NUMBUTTONS
	mouse_released = [False]*NUMBUTTONS
	state = new_state

	pygame.init()
	gui.init()
	screen = pygame.display.set_mode((800, 600), pygame.DOUBLEBUF)
	clock = pygame.time.Clock()
	running = True
	
	while running:
		for i in range(NUMKEYS):
			key_pressed[i] = False
		for i in range(NUMKEYS):
			key_released[i] = False
		for i in range(NUMBUTTONS):
			mouse_pressed[i] = False
		for i in range(NUMBUTTONS):
			mouse_released[i] = False

		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				running = False
			elif event.type == pygame.KEYDOWN:
				if event.key < 0 or event.key > NUMKEYS: continue
				key_down[event.key] = True
				key_pressed[event.key] = True
			elif event.type == pygame.KEYUP:
				if event.key < 0 or event.key > NUMKEYS: continue
				key_down[event.key] = False
				key_released[event.key] = True
			elif event.type == pygame.MOUSEBUTTONDOWN:
				if event.button < 0 or event.button > NUMBUTTONS: continue
				mouse_down[event.button] = True
				mouse_pressed[event.button] = True
			elif event.type == pygame.MOUSEBUTTONUP:
				if event.button < 0 or event.button > NUMBUTTONS: continue
				mouse_down[event.button] = False
				mouse_released[event.button] = True
		
		time_passed = clock.tick(60)
		delta = time_passed/1000.0

		state.update(delta)

		screen.fill((100, 200, 255))
		state.draw(screen)
		pygame.display.flip()
	
	state = None
	pygame.quit()