import pygame, game

hot = None
cursor = None
context = (0, 0, 0, 0)
font = None
big_font = None

def init():
	global font, big_font
	font = pygame.font.SysFont(None, 36)
	big_font = pygame.font.SysFont(None, 80)

def inside(pos, rect):
	return pos[0] >= rect[0] and pos[0] <= rect[0]+rect[2] and pos[1] >= rect[1] and pos[1] <= rect[1]+rect[3]

def offset(rect, rect2):
	return (rect[0]+rect2[0], rect[1]+rect2[1], rect[2], rect[3])

def draw_text(screen, text, pos=(0,0), color="black"):
	render = font.render(text, True, pygame.Color(color))
	rect = render.get_rect()
	rect.x, rect.y = pos
	screen.blit(render, rect)

def draw_big_text(screen, text, pos=(0,0), color="black"):
	render = big_font.render(text, True, pygame.Color(color))
	rect = render.get_rect()
	rect.x, rect.y = pos
	screen.blit(render, rect)

def draw_image(screen, image, pos=(0,0)):
	rect = image.get_rect()
	rect.x, rect.y = pos
	screen.blit(image, rect)

def draw_box(screen, rect, bgcolor="white", bordercolor="black", borderwidth=5):
	pygame.draw.rect(screen, pygame.Color(bgcolor), rect, 0)
	if borderwidth > 0:
		pygame.draw.rect(screen, pygame.Color(bordercolor), rect, borderwidth)

class Box(object):
	def __init__(self, screen, title, rect):
		self.screen = screen
		self.title = title
		self.rect = rect

	def __enter__(self):
		global context

		draw_box(self.screen, offset(self.rect, context))
		draw_text(self.screen, self.title, (self.rect[0]+context[0]+10, self.rect[1]+context[1]+10))

		self.context = context
		context = offset(offset(self.rect, self.context), (0, 50, 0, 0))

		return self

	def __exit__(self, type, value, traceback):
		global context
		context = self.context

def Menu(screen, id, text, items, rect):
	global context
	draw_box(screen, offset(rect, context))
	draw_text(screen, text, (rect[0]+context[0]+10, rect[1]+context[1]+10))

	contexty = context
	context = offset(offset(rect, contexty), (0, 50, 0, 0))

	selection = 0
	for i, item in enumerate(items):
		if Button(screen, id+str(i+1), item, (10, 10+60*i, rect[2]-20, 50)):
			selection = i+1

	context = contexty
	return selection

def Button(screen, id, text, rect, **kwargs):
	global hot
	color = "white"
	clicked = False
	if hot == id:
		if inside(game.get_mouse_pos(), offset(rect, context)):
			if game.get_mouse_released(1):
				hot = None
				clicked = True
		else:
			hot = None
		color = "grey"
	else:
		if inside(game.get_mouse_pos(), offset(rect, context)) and game.get_mouse_pressed(1) and not hot:
			hot = id
	if "draw" not in kwargs:
		draw_box(screen, offset(rect, context), color)
		draw_text(screen, text, (rect[0]+context[0]+10, rect[1]+context[1]+10))
	return clicked