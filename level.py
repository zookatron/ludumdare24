import physics

class Level():
	def __init__(self):
		self.objects = []
		self.space = pymunk.Space()

		space.gravity = (0.0, -900.0)

	def add_ball(self):
		body = physics.add_body(self.space, 1)
		physics.add_shape(body, pymunk.Circle(body, 10))
		body.position = (random.randint(120, 300), 550)

	def add_floor(self):
		body = physics.add_body(self.space)
		physics.add_shape(body, pymunk.Segment(body, (0, 0), (600.0, 0.0), 5.0))
		body.position = (0, 0)

	def draw_ball(screen, ball):
		p = int(ball.body.position.x), 600-int(ball.body.position.y)
		pygame.draw.circle(screen, (0, 0, 0), p, int(ball.radius), 2)

	def update(delta):
		self.space.step(1/60.0)

	def draw(screen):
		draw_ball(screen, ball)