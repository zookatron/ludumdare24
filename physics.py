import pymunk

dummy_body = pymunk.Body()

def limit(vec, length):
	if vec.length <= length:
		return vec
	return vec/vec.length*length

def is_empty(space, pos, collision_type=None):
	colls = space.shape_query(pymunk.Circle(dummy_body, 10))
	if collision_type:
		colls = [x for x in colls if x.shape.collision_type == collision_type]
	return len(colls) > 0

def collisions(space, a, b, collision_type=None):
	colls = space.segment_query(a, b)
	if collision_type:
		colls = [x for x in colls if x.shape.collision_type == collision_type]
	return colls

def add_body(space, mass=None):
	if mass:
		body = pymunk.Body(mass, 1)
		body.shapes = []
		space.add(body)
	else:
		body = pymunk.Body(None, None)
		body.shapes = []
	return body

def remove_body(space, body):
	for shape in body.shapes:
		remove_shape(space, shape)
	space.remove(body)

def area_of_poly(pts):
	area = 0
	nPts = len(pts)
	j = nPts-1
	i = 0
	for point in pts:
		p1 = pts[i]
		p2 = pts[j]
		area += p1.x*p2.y
		area -= p1.y*p2.x
		j = i
		i += 1
	
	area /= 2;
	return abs(area)

def area_of_circle(radius):
	return 3.14*radius*radius

def recalculate_inertia(body):
	if body.mass == 0: return
	inertia = 0
	for shape in body.shapes:
		if isinstance(shape, pymunk.Circle):
			inertia += pymunk.moment_for_circle(area_of_circle(shape.radius), 0, shape.radius, shape.offset)
		if isinstance(shape, pymunk.Poly):
			pnts = shape.get_points()
			inertia += pymunk.moment_for_poly(area_of_poly(pnts), pnts, shape.offset)
		if isinstance(shape, pymunk.Segment):
			inertia += pymunk.moment_for_segment((shape.a-shape.b).length, shape.a, shape.b)
	body.moment = body.mass*inertia

def add_shape(space, shape, **kwargs):
	shape.body.shapes.append(shape)
	if "friction" in kwargs:
		shape.friction = kwargs["friction"]
	if "collision_type" in kwargs:
		shape.collision_type = kwargs["collision_type"]
	space.add(shape)
	recalculate_inertia(shape.body)

def remove_shape(space, shape):
	shape.body.shapes.remove(shape)
	space.remove(shape)
	recalculate_inertia(shape.body)