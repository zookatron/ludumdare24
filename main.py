import game, gui, physics, pygame, pymunk, random, itertools, math, pygame.gfxdraw

COLLISION_PLAYER = 1
COLLISION_ONEWAY = 2
COLLISION_FOOD = 3
COLLISION_GROUND = 4
COLLISION_ENEMY = 5
COLLISION_BULLET = 6
random.seed(1)

GRAVITY = 500

STATE_ATTACKING = 1
STATE_PREPARING = 2
STATE_WANDERING = 3
STATE_DEAD = 4

MENU_ATTACK = 1
MENU_UTILITY = 2
MENU_MOVEMENT = 3

MAX_HEIGHT = 1000
SKY_HEIGHT = 600
WATER_HEIGHT = -600
MIN_HEIGHT = -1000

GROUNDS = 0
VINES = 1
OBJECTS = 2

class Player():
	def __init__(self, game):
		self.game = game
		self.body = physics.add_body(self.game.space, 1)
		self.body.position = (0, 0)
		self.body.velocity_func = self.playerUpdateVelocity
		self.playerShape = pymunk.Circle(self.body, 30)
		physics.add_shape(self.game.space, self.playerShape, collision_type = COLLISION_PLAYER, friction=.4)
		self.keyboard = pymunk.Vec2d(0, 0)
		self.remainingBoost = 0
		self.game.space.enable_contact_graph = True
		self.lastJumpState = False
		self.body.moment = pymunk.inf
		self.fallthrough = False
		self.grounded = False
		self.menu = None

		self.speed = 100
		self.swimspeed = 50
		self.maxspeed = 300
		self.airspeed = 100

		self.hunger = 100
		self.health = 100
		self.maxhealth = 100

		self.jumpheight = 25
		self.maxjumps = 1
		self.jumps = self.maxjumps
		self.gravity = 500

		self.ranged_damage = 20
		self.melee_damage = 40

		self.minattackspeed = .2
		self.ranged_attack_speed = .5
		self.melee_attack_speed = .4
		self.ranged_attack = 0
		self.melee_attack = 0

		self.facing = True

		self.image_body_right = resources.player_body
		self.image_body_left = pygame.transform.flip(self.image_body_right, True, False)
		self.image_leg_right = resources.player_leg
		self.image_leg_left = pygame.transform.flip(self.image_leg_right, True, False)
		self.image_wing_right = resources.player_wing
		self.image_wing_left = pygame.transform.flip(self.image_wing_right, True, False)
		self.image_jaw = resources.player_jaw_upper

		self.upgrade_attack = 0
		self.upgrade_utility = 0
		self.upgrade_movement = 0

		self.distance_travelled = 0
		self.enemies_killed = 0
		self.time = 0

		self.size = 30
		self.movin = 0
		self.flap = 0
		self.chomp = 0
		self.growth = 2

	def hit(self, amount, enemy):
		self.health -= amount

	def eat(self, food):
		self.hunger += food.value
		self.game.destroyed.append(food)

	def grow(self, amount):
		self.size += amount
		physics.remove_shape(self.game.space, self.playerShape)
		self.playerShape = pymunk.Circle(self.body, self.size)
		physics.add_shape(self.game.space, self.playerShape, collision_type = COLLISION_PLAYER, friction=.4)

	def flerpconst(self, x, y, const):
		if y > x:
			return min(x+const, y)
		elif y < x:
			return max(x-const, y)
		else:
			return x

	def get_ground_normal(self):
		self.groundNormal = pymunk.Vec2d(0, 0)
		def SelectPlayerGroundNormal(body, arbiter, groundNormal):
			n = -(pymunk.cp.cpArbiterGetNormal(arbiter, 0))
			if n.y > 0:
				self.groundNormal += n
				self.grounds += 1
		pymunk.cp.cpBodyEachArbiter(self.body._body, pymunk.cp.cpBodyArbiterIteratorFunc(SelectPlayerGroundNormal), pymunk.cp.c_void_p(0))
		return self.groundNormal

	def playerUpdateVelocity(self, body, gravity, damping, dt):
		self.groundNormal = pymunk.Vec2d()
		self.grounded = False
		self.grounds = 0
		self.get_ground_normal()
		if self.grounds > 0:
			self.groundNormal /= self.grounds

		self.grounded = self.groundNormal.y > 0.0
		#self.grounded = self.grounds > 0
		if(self.groundNormal.y < 0.0):
			self.remainingBoost = 0.0

		#Do a normal-ish update
		boost = (self.keyboard.y > 0 and self.remainingBoost > 0.0)
		g = pymunk.Vec2d(0, 0) if boost or self.grounded else pymunk.Vec2d(0, -600 if self.keyboard.y < 0 else -self.gravity)
		pymunk.Body.update_velocity(body, g, damping, dt)


		#Update the surface velocity and friction
		plane = self.groundNormal.rotated(-90)
		surface_v = plane*self.speed*self.keyboard.x - plane*plane.dot(self.body.velocity)
		self.body.velocity += surface_v
		self.playerShape.friction = 0

		#Apply air control if not grounded
		if not self.grounded:
			self.body.velocity.x = self.flerpconst(self.body.velocity.x, self.airspeed*self.keyboard.x, self.airspeed*10*dt)

		self.body.velocity = physics.limit(self.body.velocity, self.maxspeed)

	def update(self, delta):
		self.keyboard = pymunk.Vec2d(0, 0)
		if self.health > 0:
			if self.hunger > 100:
				self.hunger = 100
			if self.hunger < 0:
				self.health += self.hunger
				self.hunger = 0
			if self.health < self.maxhealth:
				self.health += self.hunger*.01*delta

			if game.get_mouse_down(3) and self.melee_attack == 0:
				vec = (pymunk.Vec2d(self.game.camera.get_mouse_pos())-self.body.position).normalized()*(self.size+30)
				colls = physics.collisions(self.game.space, self.body.position, self.body.position+vec, COLLISION_ENEMY)
				for coll in colls:
					coll.shape.body.enemy.hit(self.melee_damage, self, vec)
				self.melee_attack = self.melee_attack_speed
				self.chomp = 1
			if self.melee_attack > 0:
				self.melee_attack -= delta
			else:
				self.melee_attack = 0
			if game.get_mouse_down(1) and self.ranged_attack == 0:
				self.game.objects.append(Bullet(self.game, self.body.position, (pymunk.Vec2d(self.game.camera.get_mouse_pos())-self.body.position).normalized()*200, self.ranged_damage, self))
				self.ranged_attack = self.ranged_attack_speed
				self.chomp = 1
			if self.ranged_attack > 0:
				self.ranged_attack -= delta
			else:
				self.ranged_attack = 0
			if self.flap > 0:
				self.flap -= delta
			else:
				self.flap = 0
			if self.chomp > 0:
				self.chomp -= delta*3
			else:
				self.chomp = 0

			if game.get_key_down(pygame.K_a):
				self.facing = False
				self.keyboard.x -= 1
			if game.get_key_down(pygame.K_d):
				self.facing = True
				self.keyboard.x += 1
			if game.get_key_down(pygame.K_w):
				self.keyboard.y += 1
			if game.get_key_down(pygame.K_s):
				self.keyboard.y -= 1

			self.fallthrough = self.keyboard.y < 0.0
			self.hunger -= 2*delta;

			if self.grounded:
				self.jumps = self.maxjumps
			if self.remainingBoost > 0:
				self.remainingBoost -= delta
			else:
				self.remainingBoost = 0
			if game.get_key_pressed(pygame.K_w) and self.jumps > 0:
				jump_v = pow(2.0*self.jumpheight/2*GRAVITY, .5)
				#self.body.velocity += (self.groundNormal if self.groundNormal.length > 0 else pymunk.Vec2d(0, 1))*jump_v
				self.body.velocity = pymunk.Vec2d(0, jump_v)
				self.remainingBoost = self.jumpheight/jump_v
				self.jumps -= 1
				self.flap = 1

			self.distance_travelled += self.body.velocity.length*delta
			self.movin += self.body.velocity.x*delta
			self.time += delta
			if(self.distance_travelled > 1000):
				self.upgrade_movement += 1
				self.distance_travelled = 0
			if(self.enemies_killed > 5):
				self.upgrade_attack += 1
				self.enemies_killed = 0
			if(self.time > 30):
				self.upgrade_utility += 1
				self.time = 0

	def draw(self, screen):
		#self.game.camera.draw_circle(screen, self.playerShape.radius, self.body.position)

		if self.health <= 0:
			return self.game.camera.draw_image(screen, pygame.transform.rotozoom(resources.player_dead, 0, self.playerShape.radius*.0025), (self.body.position.x-self.playerShape.radius*.8, self.body.position.y+self.playerShape.radius*.9))

		scale = self.size*.0025

		body = self.image_body_right if self.facing else self.image_body_left
		leg = self.image_leg_right if self.facing else self.image_leg_left
		wing = self.image_wing_right if self.facing else self.image_wing_left
		jaw = self.image_jaw

		self.game.camera.draw_image(screen, pygame.transform.rotozoom(body, 0, scale), (self.body.position.x-self.size*.8, self.body.position.y+self.size*.9))

		new_leg = pygame.transform.rotozoom(leg, math.cos(self.movin*.1)*45, scale)
		self.game.camera.draw_image(screen, new_leg, (self.body.position.x+self.size*.4-(leg.get_width()*scale+new_leg.get_width())*.5, self.body.position.y-self.size*.0-(leg.get_height()*scale-new_leg.get_height())*.5))

		new_wing = pygame.transform.rotozoom(wing, (1 if self.facing else -1)*self.flap*45, scale)
		self.game.camera.draw_image(screen, new_wing, (self.body.position.x+(-self.size*0.5 if self.facing else self.size*1.2)-(wing.get_width()*scale+new_wing.get_width())*.5, self.body.position.y+self.size*.7-(wing.get_height()*scale-new_wing.get_height())*.5))

		new_lower_jaw = pygame.transform.rotozoom(jaw, 30-self.chomp*45+(0 if self.facing else 180), scale)
		self.game.camera.draw_image(screen, new_lower_jaw, (self.body.position.x+(self.size*1.0 if self.facing else -self.size*0.5)-(jaw.get_width()*scale+new_lower_jaw.get_width())*.5, self.body.position.y+(self.size*.6 if self.facing else self.size*.4)-(jaw.get_height()*scale-new_lower_jaw.get_height())*.5))

		new_upper_jaw = pygame.transform.rotozoom(jaw, -30+self.chomp*45+(0 if self.facing else 180), scale)
		self.game.camera.draw_image(screen, new_upper_jaw, (self.body.position.x+(self.size*1.0 if self.facing else -self.size*0.5)-(jaw.get_width()*scale+new_lower_jaw.get_width())*.5, self.body.position.y+(self.size*.4 if self.facing else self.size*.6)-(jaw.get_height()*scale-new_lower_jaw.get_height())*.5))

	def gui(self, screen):
		if self.health <= 0:
			gui.draw_big_text(screen, "YOU DIED! :(", (198, 298), "white")
			gui.draw_big_text(screen, "YOU DIED! :(", (200, 300), "black")
			return gui.draw_text(screen, "press escape", (300, 400))

		gui.draw_box(screen, (20, 20, 20+max(min(self.hunger, 100), 0)*2, 50), "blue")
		gui.draw_box(screen, (screen.get_width()-(20+max(min(self.health, 100), 0)*2), 20, max(min(self.health, 100), 0)*2, 50), "green")



		if self.upgrade_attack > 0:
			if gui.Button(screen, "upgrade_attack", "a", (300, 20, 50, 50), draw=False):
				if self.menu:
					self.menu = None
				else:
					self.menu = MENU_ATTACK
			gui.draw_image(screen, resources.attack_icon, (300, 20))
		if self.upgrade_utility > 0:
			if gui.Button(screen, "upgrade_utility", "u", (360, 20, 50, 50), draw=False):
				if self.menu:
					self.menu = None
				else:
					self.menu = MENU_UTILITY
			gui.draw_image(screen, resources.utility_icon, (360, 20))
		if self.upgrade_movement > 0:
			if gui.Button(screen, "upgrade_movement", "m", (420, 20, 50, 50), draw=False):
				if self.menu:
					self.menu = None
				else:
					self.menu = MENU_MOVEMENT
			gui.draw_image(screen, resources.movement_icon, (420, 20))


		if self.menu == MENU_ATTACK:
			selection = gui.Menu(screen, "upgrade_attack", "evolve", ["ranged", "melee"], (300, 80, 170, 180))
			if selection == 1:
				if self.ranged_damage < 50:
					self.ranged_damage += 10
					self.game.send_message("more ranged damage!")
				else:
					if self.ranged_attack_speed > self.minattackspeed:
						self.ranged_attack_speed -= .1
						self.game.send_message("more ranged attack speed!")
				self.upgrade_attack -= 1
				self.grow(self.growth)
				self.menu = None
			if selection == 2:
				if self.melee_damage < 60:
					self.melee_damage += 10
					self.game.send_message("more melee damage!")
				else:
					if self.melee_damage_speed > self.minattackspeed-.1:
						self.melee_damage_speed -= .1
						self.game.send_message("more melee attack speed!")
				self.upgrade_attack -= 1
				self.grow(self.growth)
				self.menu = None

		if self.menu == MENU_UTILITY:
			selection = gui.Menu(screen, "upgrade_utility", "evolve", ["health"], (300, 80, 170, 120))
			if selection == 1:
				self.health += 10
				self.maxhealth += 10
				self.game.send_message("more health!")
				self.upgrade_utility -= 1
				self.grow(self.growth)
				self.menu = None

		if self.menu == MENU_MOVEMENT:
			selection = gui.Menu(screen, "upgrade_movement", "evolve", ["jumping", "running"], (300, 80, 170, 180))
			if selection == 1:
				if self.maxjumps == 1:
					self.jumpheight *= 2
					self.maxjumps += 1
					self.game.send_message("double jumps!")
				elif self.maxjumps == 2:
					self.maxjumps = 100000
					self.game.send_message("infinite jumps!")
				else:
					self.jumpheight += 10
					self.airspeed += 30
					self.game.send_message("better flying!")
				self.upgrade_movement -= 1
				self.grow(self.growth)
				self.menu = None
			if selection == 2:
				self.speed += 50
				self.game.send_message("faster running!")
				self.upgrade_movement -= 1
				self.grow(self.growth)
				self.menu = None
			if selection == 3:
				self.swimspeed += 50
				self.upgrade_movement -= 1
				self.grow(self.growth)
				self.menu = None

class Bullet():
	def __init__(self, game, pos, vel, damage, creator, image=None):
		self.creator = creator
		self.damage = damage
		self.game = game
		self.body = physics.add_body(self.game.space, 10)
		self.body.bullet = self
		self.body.position = pos
		self.body.velocity = vel
		self.body.velocity_func = self.update_velocity
		physics.add_shape(self.game.space, pymunk.Circle(self.body, 10), collision_type=COLLISION_BULLET, friction=0.0)
		if not image:
			self.image = pygame.transform.rotozoom(resources.bullet, 0, .01)

	def update_velocity(self, body, gravity, damping, dt):
		pymunk.Body.update_velocity(body, pymunk.Vec2d(0, 0), damping, dt)

	def destroy(self):
		if not self.game: return
		self.body.bullet = None
		physics.remove_body(self.game.space, self.body)
		self.body = None
		self.game.objects.remove(self)
		self.game = None

	def update(self, delta):
		pass

	def draw(self, screen):
		#self.game.camera.draw_circle(screen, 5, self.body.position)
		self.game.camera.draw_image(screen, self.image, (self.body.position.x-self.image.get_width()/2, self.body.position.y+self.image.get_height()/2))

class Broccoli():
	def __init__(self, game, pos):
		self.game = game
		self.body = physics.add_body(self.game.space, 1)
		self.body.enemy = self
		self.body.position = pos
		self.body.velocity_func = self.update_velocity
		physics.add_shape(self.game.space, pymunk.Circle(self.body, 20), collision_type=COLLISION_ENEMY, friction=0.5)
		self.speed = 80
		self.maxspeed = 300
		self.range = 100
		self.canseeplayer = False
		self.state = STATE_WANDERING
		self.attack_timer = 0
		self.attack_time = 3
		self.damage = 10
		scale = .05
		self.image_body = pygame.transform.smoothscale(resources.broccoli_body, (int(resources.wasp_body.get_width()*scale), int(resources.wasp_body.get_height()*scale)))
		self.image_dead = pygame.transform.flip(self.image_body, False, True)
		self.facing = True
		self.health = 50
		self.keyboard = pymunk.Vec2d()

	def hit(self, amount, enemy, knockback=None):
		self.health -= amount
		if knockback:
			self.body.velocity += knockback

	def destroy(self):
		self.body.enemy = None
		physics.remove_body(self.game.space, self.body)
		self.body = None
		self.game.objects.remove(self)
		self.game = None

	def player_hit(self):
		if self.state == STATE_ATTACKING:
			self.game.player.hit(self.damage, self)
			self.state = STATE_PREPARING


	def get_ground_normal(self):
		self.groundNormal = pymunk.Vec2d(0, 0)
		def SelectGroundNormal(body, arbiter, groundNormal):
			n = -(pymunk.cp.cpArbiterGetNormal(arbiter, 0))
			if n.y > 0:
				self.groundNormal += n
				self.grounds += 1
		pymunk.cp.cpBodyEachArbiter(self.body._body, pymunk.cp.cpBodyArbiterIteratorFunc(SelectGroundNormal), pymunk.cp.c_void_p(0))
		return self.groundNormal

	def update_velocity(self, body, gravity, damping, dt):
		self.groundNormal = pymunk.Vec2d()
		self.grounded = False
		self.grounds = 0
		self.get_ground_normal()
		if self.grounds > 0:
			self.groundNormal /= self.grounds
			self.grounded = True

		pymunk.Body.update_velocity(body, gravity, damping, dt)

		#Update the surface velocity and friction
		plane = self.groundNormal.rotated(-90)
		surface_v = plane*self.speed*self.keyboard.x - plane*plane.dot(self.body.velocity)
		self.body.velocity += surface_v

	def update(self, delta):
		playerdist = (self.body.position-self.game.player.body.position).length
		if playerdist < 500:
			if random.random() < .1 and self.state != STATE_DEAD:
				self.canseeplayer = len(physics.collisions(self.game.space, self.body.position, self.game.player.body.position, COLLISION_GROUND)) == 0
		else:
			self.canseeplayer = False

		if self.state != STATE_DEAD and self.health <= 0:
			self.game.player.enemies_killed += 1
			self.keyboard = pymunk.Vec2d(0, 0)
			self.state = STATE_DEAD
		if self.state == STATE_WANDERING:
			self.keyboard = pymunk.Vec2d(random.random()-0.5, 0)
			#self.body.velocity += pymunk.Vec2d(*10-5, random.random()*10-5)
			if self.canseeplayer:
				self.state = STATE_PREPARING
		if self.state == STATE_PREPARING:
			self.facing = self.body.position.x<self.game.player.body.position.x
			if playerdist < self.range*2:
				self.attack_timer += delta
			if self.attack_timer > self.attack_time:
				self.attack_timer = 0
				self.state = STATE_ATTACKING
			flip = -1 if playerdist < self.range else 1
			self.keyboard = pymunk.Vec2d(flip*math.copysign(1, (self.game.player.body.position-self.body.position).x), 0)
			#self.body.velocity += flip*(self.body.position-self.game.player.body.position).normalized()*-self.speed*2*delta
			if not self.canseeplayer:
				self.state = STATE_WANDERING
		if self.state == STATE_ATTACKING:
			self.facing = self.body.position.x<self.game.player.body.position.x
			self.keyboard = pymunk.Vec2d(math.copysign(1, (self.game.player.body.position-self.body.position).x), 0)
			#self.body.velocity += (self.body.position-self.game.player.body.position).normalized()*-self.speed*2*delta
			if not self.canseeplayer:
				self.state = STATE_WANDERING
	
		if self.state != STATE_DEAD:
			self.body.velocity = physics.limit(self.body.velocity, self.maxspeed)

	def draw(self, screen):
		#self.game.camera.draw_circle(screen, 20, self.body.position)
		self.game.camera.draw_image(screen, self.image_dead if self.state == STATE_DEAD else self.image_body, (self.body.position.x-20, self.body.position.y+30))

class Wasp():
	def __init__(self, game, pos):
		self.game = game
		self.body = physics.add_body(self.game.space, 1)
		self.body.enemy = self
		self.body.position = pos
		self.body.velocity_func = self.update_velocity
		physics.add_shape(self.game.space, pymunk.Circle(self.body, 20), collision_type=COLLISION_ENEMY, friction=0.5)
		self.speed = 30
		self.range = 100
		self.canseeplayer = False
		self.state = STATE_WANDERING
		self.attack_timer = 0
		self.attack_time = 3
		self.damage = 10
		scale = .05
		self.facing = True
		self.health = 30

		self.image_body_right = pygame.transform.smoothscale(resources.wasp_body, (int(resources.wasp_body.get_width()*scale), int(resources.wasp_body.get_height()*scale)))
		self.image_body_left = pygame.transform.flip(self.image_body_right, True, False)
		self.image_dead = pygame.transform.flip(self.image_body_right, False, True)
		self.image_wing_right = pygame.transform.smoothscale(resources.wasp_wing, (int(resources.wasp_wing.get_width()*scale), int(resources.wasp_wing.get_height()*scale)))
		self.image_wing_left = pygame.transform.flip(self.image_wing_right, True, False)

	def hit(self, amount, enemy, knockback=None):
		self.health -= amount
		if knockback:
			self.body.velocity += knockback

	def destroy(self):
		self.body.enemy = None
		physics.remove_body(self.game.space, self.body)
		self.body = None
		self.game.objects.remove(self)
		self.game = None

	def update_velocity(self, body, gravity, damping, dt):
		pymunk.Body.update_velocity(body, pymunk.Vec2d(0, 0) if self.state != STATE_DEAD else gravity, damping, dt)

	def player_hit(self):
		if self.state == STATE_ATTACKING:
			self.game.player.hit(self.damage, self)
			self.state = STATE_PREPARING

	def update(self, delta):
		if random.random() < .1 and self.state != STATE_DEAD:
			self.canseeplayer = len(physics.collisions(self.game.space, self.body.position, self.game.player.body.position, COLLISION_GROUND)) == 0

		if self.state != STATE_DEAD and self.health <= 0:
			self.game.player.enemies_killed += 1
			self.state = STATE_DEAD
		if self.state == STATE_WANDERING:
			self.body.velocity += pymunk.Vec2d(random.random()*10-5, random.random()*10-5)
			if self.canseeplayer:
				self.state = STATE_PREPARING
		if self.state == STATE_PREPARING:
			self.facing = self.body.position.x<self.game.player.body.position.x
			playerdist = (self.body.position-self.game.player.body.position).length
			if playerdist < self.range*2:
				self.attack_timer += delta
			if self.attack_timer > self.attack_time:
				self.attack_timer = 0
				self.state = STATE_ATTACKING
			flip = -1 if playerdist < self.range else 1
			self.body.velocity += flip*(self.body.position-self.game.player.body.position).normalized()*-self.speed/10
			if not self.canseeplayer:
				self.state = STATE_WANDERING
		if self.state == STATE_ATTACKING:
			self.facing = self.body.position.x<self.game.player.body.position.x
			self.body.velocity += (self.body.position-self.game.player.body.position).normalized()*-self.speed/2
			if not self.canseeplayer:
				self.state = STATE_WANDERING
	
		if self.state != STATE_DEAD:
			self.body.velocity = physics.limit(self.body.velocity, self.speed)

	def draw(self, screen):
		#self.game.camera.draw_circle(screen, 20, self.body.position)
		self.game.camera.draw_image(screen, self.image_dead if self.state == STATE_DEAD else (self.image_body_right if self.facing else self.image_body_left), (self.body.position.x-20, self.body.position.y+30))
		if self.state != STATE_DEAD:
			self.game.camera.draw_image(screen, pygame.transform.rotozoom(self.image_wing_right if self.facing else self.image_wing_left, math.cos(self.game.time*50)*45, 1), (self.body.position.x+(-50 if self.facing else 10), self.body.position.y+30))

class Flower():
	def __init__(self, game, pos):
		self.orig_texture = resources.flower
		self.texture = pygame.transform.rotozoom(self.orig_texture, 0, .5)
		self.game = game
		self.body = physics.add_body(self.game.space, .1)
		self.body.position = pos
		self.stem = pos
		shape = pymunk.Circle(self.body, 10)
		shape.collision_type = COLLISION_FOOD
		physics.add_shape(self.game.space, shape, friction=0.5)
		self.body.food = self
		self.speed = 10
		self.body.velocity_func = self.update_velocity
		self.value = 10

	def update_velocity(self, body, gravity, damping, dt):
		self.body.velocity += pymunk.Vec2d(random.random()*self.speed-self.speed/2, random.random()*self.speed-self.speed/2)
		self.body.velocity.y = min(max(self.body.velocity.y, -self.speed), self.speed)
		self.body.velocity.x = min(max(self.body.velocity.x, -self.speed), self.speed)
		pymunk.Body.update_velocity(body, pymunk.Vec2d(0, 0), damping, dt)

	def destroy(self):
		self.body.food = None
		physics.remove_body(self.game.space, self.body)
		self.body = None
		self.game.objects.remove(self)
		self.game = None

	def update(self, delta):
		pass

	def draw(self, screen):
		#self.game.camera.draw_circle(screen, 5, self.body.position)
		self.game.camera.draw_line(screen, pymunk.Vec2d(self.body.position), pymunk.Vec2d(self.stem), (0, 0), (0, 200, 0), 2)
		self.game.camera.draw_image(screen, self.texture, (self.body.position.x-self.texture.get_width()/2, self.body.position.y+self.texture.get_height()/2))

#GroundShapes = [("rock1.png", [[60,520,53,404,148,307,335,319,531,385,547,504,466,547], [63,527,273,539,142,562], [164,302,238,191,289,156,325,315], [311,229,486,127,513,78,484,43,327,40,286,67,286,118]])]
# ("data\\Environment\\rock1.png", [[263,532,167,560,123,559,73,540,57,517,50,407,121,321,204,250,312,261,351,330],
# 								[327,227,305,268,202,254,233,193,286,157],
# 								[292,131,480,134,323,229,283,161],
# 								[282,91,294,135,480,132,521,95,521,63,496,39,333,37,299,49,282,69],
# 								[524,377,534,389,549,492,518,532,477,545,359,548,254,529,347,328]]),
GroundShapes = [("data\\Environment\\rock2.png", [[178,370,247,229,205,179,126,159,7,271,2,311,11,363,52,385,109,391,137,385],
								[384,258,394,339,369,369,264,381,175,368,240,225],
								[217,149,203,186,125,162,146,124,187,96],
								[191,77,186,99,216,152,342,80],
								[186,37,186,56,196,81,341,79,366,63,376,31,358,11,221,8,196,20]]),
				("data\\Environment\\branch.png", [[49,491,32,407,39,285,118,281,117,416,92,485],
								[1,175,49,177,128,189,129,226,117,281,38,280,12,232],
								[134,477,119,485,94,483,118,416],
								[54,124,63,76,93,12,124,3,129,86,126,191,52,180],
								[51,11,62,81,94,13]])]

class Ground():
	def __init__(self, game, pos):
		self.game = game
		self.body = physics.add_body(self.game.space, None)
		self.body.position = pos
		self.body.ground = self
		self.body.angle = random.random()*3.14

		#self.points = convexhull([(random.random()*500-200, random.random()*50-25) for x in range(10)])
		shape = GroundShapes[int(math.floor(random.uniform(0, len(GroundShapes))))]

		self.texture = shape[0]
		self.orig_tex = self.texture
		self.xx = int(self.texture.get_width()/2)
		self.yy = int(self.texture.get_height()/2)
		for x in shape[1]:
			pnts = [(x[i]-self.xx, -x[i+1]+self.yy) for i in range(len(x)) if i % 2 == 0]
			shapez = pymunk.Poly(self.body, pnts)
			shapez.collision_type = COLLISION_GROUND
			physics.add_shape(self.game.space, shapez, friction=1.0)

		self.texture = pygame.transform.rotozoom(self.orig_tex, self.body.angle/3.14*180, 1)
		self.scr_corr = (self.xx+(self.texture.get_width()-self.orig_tex.get_width())/2, self.yy+(self.texture.get_height()-self.orig_tex.get_height())/2)

	def get_point(self):
		shape = self.body.shapes[int(math.floor(random.uniform(0, len(self.body.shapes))))]
		points = shape.get_points()
		return random.choice(points)

	def draw(self, screen):
		#self.game.camera.draw_circle(screen, 10, self.body.position)
		self.game.camera.draw_image(screen, self.texture, (self.body.position.x-self.scr_corr[0], self.body.position.y+self.scr_corr[1]))
		# for shape in self.body.shapes:
		# 	self.game.camera.draw_poly(screen, shape.get_points(), (0, 0))

class Vine():
	def __init__(self, game, a, b):
		self.game = game
		self.body = physics.add_body(self.game.space, None)
		if a.x > b.x:
			a, b = b, a
		self.shape = pymunk.Segment(self.body, a, b, 2)
		self.shape.collision_type = COLLISION_ONEWAY
		physics.add_shape(self.game.space, self.shape, friction=1.0)

	def update(self, delta):
		pass

	def draw(self, screen):
		self.game.camera.draw_line(screen, self.shape.a, self.shape.b, self.body.position, (0, 128, 0), 5)

class Camera():
	def __init__(self):
		self.following = None
		self.rect = (0, 0, 800, 600)
		self.x_scaling = 800/self.rect[2]
		self.y_scaling = 600/self.rect[3]

	def get_mouse_pos(self):
		mouse = game.get_mouse_pos()
		return (self.rect[0]+mouse[0], self.rect[1]-(mouse[1]-self.rect[3]))

	def convertx(self, point):
		return int((point-self.rect[0])*self.x_scaling)

	def converty(self, point):
		return int((self.rect[3]-(point-self.rect[1]))*self.y_scaling)

	def follow(self, obj):
		self.following = obj

	def draw_circle(self, screen, radius, pos):
		pygame.draw.circle(screen, pygame.Color("black"), (self.convertx(pos[0]), self.converty(pos[1])), int(radius*self.y_scaling))

	def draw_poly(self, screen, pnts, pos):
		pygame.draw.polygon(screen, pygame.Color("black"), [(self.convertx(pos[0]+p[0]), self.converty(pos[1]+p[1])) for p in pnts])

	def draw_line(self, screen, a, b, pos=(0, 0), color=(0,0,0), radius=1):
		pygame.draw.line(screen, pygame.Color(color[0], color[1], color[2], 1), (self.convertx(a.x+pos[0]), self.converty(a.y+pos[1])), (self.convertx(b.x+pos[0]), self.converty(b.y+pos[1])), radius)

	def draw_image(self, screen, tex, pos):
		gui.draw_image(screen, tex, (self.convertx(pos[0]), self.converty(pos[1])))

	def draw_square(self, screen, pos, size):
		pygame.draw.rect(screen, pygame.Color("black"), (self.convertx(pos[0]*size), self.converty(pos[1]*size)-size, size, size))

	def update(self, delta):
		if self.following:
			self.rect = (self.following.body.position.x-self.rect[2]/2, self.following.body.position.y-self.rect[3]/2, self.rect[2], self.rect[3])

	def draw_bg(self, screen, tex):
		iwidth = tex.get_width()
		iheight = tex.get_height()
		swidth = screen.get_width()
		sheight = screen.get_height()

		speed = .5
		startx = -self.rect[0]*self.x_scaling*speed
		starty = self.rect[1]*self.y_scaling*speed
		while startx < -iwidth:
			startx += iwidth
		while starty < -iheight:
			starty += iheight
		while startx > 0:
			startx -= iwidth
		while starty > 0:
			starty -= iheight

		x = startx
		while x < swidth:
			y = starty
			while y < sheight:
				gui.draw_image(screen, tex, (x, y))
				y += iheight
			x += iwidth

class GameState():
	def __init__(self):
		self.space = pymunk.Space()
		self.space.gravity = (0.0, -GRAVITY)
		self.space.add_collision_handler(COLLISION_ONEWAY, COLLISION_PLAYER, None, self.oneway_presolve, None, None, None)
		self.space.add_collision_handler(COLLISION_FOOD, COLLISION_PLAYER, None, self.food_presolve, None, None, None)
		self.space.add_collision_handler(COLLISION_ENEMY, COLLISION_ONEWAY, None, self.enemy_oneway, None, None, None)
		self.space.add_collision_handler(COLLISION_ENEMY, COLLISION_PLAYER, None, self.player_hit, None, None, None)
		self.space.add_collision_handler(COLLISION_BULLET, COLLISION_PLAYER, None, self.bullet_player, None, None, None)
		self.space.add_collision_handler(COLLISION_BULLET, COLLISION_ENEMY, None, self.bullet_enemy, None, None, None)
		self.space.add_collision_handler(COLLISION_BULLET, COLLISION_ONEWAY, None, lambda *args: False, None, None, None)
		self.space.add_collision_handler(COLLISION_BULLET, COLLISION_GROUND, None, self.kill_bullet, None, None, None)

		self.player = Player(self)
		self.camera = Camera()
		self.camera.follow(self.player)

		self.objects = []
		self.destroyed = []
		self.grounds = []
		self.vines = []
		self.squares = {}
		self.SQUARE_SIZE = 500
		self.show_info = False
		self.delta = 0
		self.messages = []
		self.message_timeout = 5
		self.time = 0

		self.grounds.append(Ground(self, (50, 0)))

	def enemy_oneway(self, space, arb, *args, **kwargs):
		if isinstance(arb.shapes[0].body.enemy, Wasp): return False
		return True

	def kill_bullet(self, space, arb, *args, **kwargs):
		bullet = arb.shapes[0].body.bullet
		self.destroyed.append(bullet)
		return True

	def bullet_player(self, space, arb, *args, **kwargs):
		bullet = arb.shapes[0].body.bullet
		if bullet.creator is not self.player:
			self.player.hit(bullet.damage, bullet.creator)
			self.destroyed.append(bullet)
			return True
		else:
			return False

	def bullet_enemy(self, space, arb, *args, **kwargs):
		bullet = arb.shapes[0].body.bullet
		enemy = arb.shapes[1].body.enemy
		if bullet.creator is not enemy:
			enemy.hit(bullet.damage, bullet.creator)
			self.destroyed.append(bullet)
			return True
		else:
			return False

	def genvine(self, ground, othergrounds):
		a = ground
		b = random.choice(othergrounds)

		ap = a.get_point()
		bp = b.get_point()

		for x in range(50):
			if ap == bp or (ap-bp).length < 50 or ap.x == bp.x or abs(ap.y-bp.y)/abs(ap.x-bp.x) > 0.5:
				ap = a.get_point()
				b = random.choice(othergrounds)
				bp = b.get_point()
				continue
			break
		else:
			return None

		return Vine(self, ap, bp)

	def oneway_presolve(self, space, arb, *args, **kwargs):
		segment = arb.shapes[0]

		if self.player.fallthrough or (segment.a-segment.b).rotated(90).dot(arb.contacts[0].normal) < 0:
			return False

		return True

	def food_presolve(self, space, arb, *args, **kwargs):
		self.player.eat(arb.shapes[0].body.food)
		return False

	def player_hit(self, space, arb, *args, **kwargs):
		arb.shapes[0].body.enemy.player_hit()
		return True

	def get_free_spot(self, x, y):
		spot = None
		while not spot or not physics.is_empty(self.space, spot):
			spot = ((x+random.random())*self.SQUARE_SIZE, (y+random.random())*self.SQUARE_SIZE)
		return spot

	def gensquare(self, x, y, surrounding):
		square = [[], [], []]
		for i in range(1):
			ground = Ground(self, ((x+random.random())*self.SQUARE_SIZE, (y+random.random())*self.SQUARE_SIZE))
			square[GROUNDS].append(ground)
			self.grounds.append(ground)
		surrounding_grounds = []
		for surr in surrounding:
			surrounding_grounds += surr[GROUNDS]
		if not len(surrounding_grounds) == 0:
			for ground in square[GROUNDS]:
				for i in range(3):
					vine = self.genvine(ground, surrounding_grounds)
					if not vine: continue
					square[VINES].append(vine)
					self.vines.append(vine)
		for i in range(2):
			enemy_type = random.choice([Wasp, Broccoli])
			enemy = enemy_type(self, self.get_free_spot(x, y))
			square[OBJECTS].append(enemy)
			self.objects.append(enemy)
		for i in range(3):
			food = Flower(self, random.choice(square[GROUNDS]).get_point())
			square[OBJECTS].append(food)
			self.objects.append(food)
		return square

	def hash(self, pos):
		return 100000*pos[0]+pos[1]

	def get_surrounding(self, pos):
		returns = []
		for x in range(-1, 2):
			for y in range(-1, 2):
				square = self.hash((pos[0]+x, pos[1]+y))
				if square in self.squares:
					returns.append(self.squares[square])
		return returns

	def getsquares(self, rect):
		returns = []

		startx = int(math.floor((rect[0])/self.SQUARE_SIZE))-1
		starty = int(math.floor((rect[1])/self.SQUARE_SIZE))-1

		endx = int(math.floor((rect[0]+rect[2])/self.SQUARE_SIZE))+1
		endy = int(math.floor((rect[1]+rect[3])/self.SQUARE_SIZE))+1


		x = startx
		while x <= endx:
			y = starty
			while y <= endy:
				square = self.hash((x, y))
				if square not in self.squares:
					self.squares[square] = self.gensquare(x, y, self.get_surrounding((x, y)))
				returns.append(self.squares[square])
				y += 1
			x += 1

		return returns		

	def update(self, delta):
		self.time += delta
		self.delta = delta
		self.current_squares = self.getsquares(self.camera.rect)

		for obj in self.objects:
			obj.update(delta)

		self.player.update(delta)

		self.camera.update(delta)
		self.space.step(delta)

		if len(self.destroyed) > 0:
			for obj in self.destroyed:
				obj.destroy()
			self.destroyed = []

		if game.get_key_pressed(pygame.K_ESCAPE):
			game.set_state(MenuState())

		for message in self.messages:
			message[1] -= delta
			if message[1] < 0:
				self.messages.remove(message)

	def send_message(self, message):
		self.messages.append([message, self.message_timeout])

	def draw(self, screen):
		#self.camera.draw_bg(screen, resources.leafy)

		for vine in self.vines:
			vine.draw(screen)
		for ground in self.grounds:
			ground.draw(screen)
		for obj in self.objects:
			obj.draw(screen)

		self.player.draw(screen)

		self.player.gui(screen)

		for i, message in enumerate(self.messages):
			gui.draw_text(screen, message[0], (20, screen.get_height()-30-30*i), "red")

		if game.get_key_pressed(pygame.K_F1):
			self.show_info = not self.show_info
		if self.show_info:
			gui.draw_text(screen, str(1/self.delta))

class MenuState():
	def update(self, delta):
		pass

	def draw(self, screen):
		gui.draw_image(screen, resources.splash)
		with gui.Box(screen, "Main Menu", (450, 300, 220, 240)):
			if gui.Button(screen, "play", "play", (10, 10, 200, 50)):
				game.set_state(GameState())
			if gui.Button(screen, "help", "help", (10, 70, 200, 50)):
				game.set_state(HelpState())
			if gui.Button(screen, "quit", "quit", (10, 130, 200, 50)):
				game.quit()

class HelpState():
	def update(self, delta):
		pass

	def draw(self, screen):
		gui.draw_image(screen, resources.help)
		if gui.Button(screen, "back", "back", (300, 540, 200, 50)):
			game.set_state(MenuState())

class ResourceState():
	def update(self, delta):
		self.leafy = pygame.image.load("data\\Environment\\Sky.png")
		self.flower = pygame.image.load("data\\Environment\\flower.png")
		self.wasp_body = pygame.image.load("data\\Characters\\Enemies\\Jalapeno\\Body.png")
		self.wasp_wing = pygame.image.load("data\\Characters\\Enemies\\Jalapeno\\Wing Front.png")
		self.broccoli_body = pygame.image.load("data\\Characters\\Enemies\\Onion\\Onion.png")
		self.player_body = pygame.image.load("data\\Characters\\Main Character\\Body.png")
		self.player_leg = pygame.image.load("data\\Characters\\Main Character\\Leg Front.png")
		self.player_wing = pygame.image.load("data\\Characters\\Main Character\\Wing Front.png")
		self.player_jaw_upper = pygame.image.load("data\\Characters\\Main Character\\Jaw Upper.png")
		self.player_jaw_lower = pygame.image.load("data\\Characters\\Main Character\\Jaw Lower.png")
		self.player_dead = pygame.image.load("data\\Characters\\Main Character\\Dead.png")
		self.bullet = pygame.image.load("data\\Characters\\Main Character\\Bullet.png")
		self.splash = pygame.image.load("data\\Menu\\SplashScreen.png")
		self.help = pygame.image.load("data\\Menu\\help.png")
		self.attack_icon = pygame.image.load("data\\Menu\\Attack.png")
		self.utility_icon = pygame.image.load("data\\Menu\\Utility.png")
		self.movement_icon = pygame.image.load("data\\Menu\\Movement.png")
		for i in range(len(GroundShapes)):
			GroundShapes[i] = (pygame.image.load(GroundShapes[i][0]), GroundShapes[i][1])
		game.set_state(MenuState())

resources = ResourceState()
game.run(resources)

# import profile, pstats
# profile.run('game.run(resources)', "stats")
# stats = pstats.Stats('stats')
# stats.sort_stats('cumulative')
# stats.print_stats()